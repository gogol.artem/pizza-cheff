import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const pizzas = [
      {
        id: 1,
        title: 'Мясная Делюкс',
        text: 'Пепперони, лук, бекон, томатная паста, колбаски, перец, грибы',
      },
      {
        id: 2,
        title: 'Морская Премиум',
        text: 'Перец, сыр, креветки, кальмары, мидии, лосось',
      },
      {
        id: 3,
        title: 'Бекон и Сосиски',
        text: 'Бекон, сыр, сосиски, ананас, томатная паста',
      },
      {
        id: 4,
        title: 'Куриная Делюкс',
        text: 'Курица, ананас, сыр Пепперони, соус для пиццы, томатная паста',
      },
      {
        id: 5,
        title: 'Барбекю Премиум',
        text: 'Свинина BBQ, соус Барбкею, сыр, курица, соус для пиццы, соус чи',
      },
      {
        id: 6,
        title: 'Пепперони Дабл',
        text: 'Пепперони, сыр, колбаса 2 видов: обжаренная и вареная',
      },
      {
        id: 7,
        title: 'Куриное трио',
        text: 'Жареная курица, Тушеная курица, Куриные наггетсы, перец, сыр, г',
      },
      {
        id: 8,
        title: 'Сырная',
        text: 'Сыр Джюгас, Сыр с плесенью, Сыр Моцарелла, Сыр секретный',
      },
    ]
    return { pizzas }
  }
}
