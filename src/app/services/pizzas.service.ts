import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPizza } from "../interfaces/pizza";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {
  private pizzasUrl = 'api/pizzas';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
  ) {}

  getPizzas(): Observable<IPizza[]> {
    return this.http.get<IPizza[]>(this.pizzasUrl)
  }
}
