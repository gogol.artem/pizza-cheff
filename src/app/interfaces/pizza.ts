export interface IPizza {
  id: number,
  title: string,
  text: string
}
