import { Component, OnInit } from '@angular/core';
import { IPizza } from "../../interfaces/pizza";
import { PizzasService } from '../../services/pizzas.service'

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.scss']
})
export class PizzasComponent implements OnInit {
  pizzas: IPizza[] = [];

  constructor(
    private pizzasService: PizzasService
  ) {}

  ngOnInit(): void {
    this.getPizzas();
  }

  getPizzas(): void {
    this.pizzasService.getPizzas()
      .subscribe(pizzas => this.pizzas = pizzas)
  }
}
